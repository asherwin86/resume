import dynamic from 'next/dynamic';

const HeaderComponent = dynamic(() => import('../components/Header'));


function Home() {
  return (
        <div>
        <HeaderComponent />
        </div>
  	);
}

export default Home;
