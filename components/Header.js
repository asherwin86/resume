import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import { withStyles } from '@material-ui/core/styles';

import headerJss from "../styles/headerJss";

function Header() {
    return (
        <div>
            <AppBar>
                <Toolbar>
                    <Typography variant="h6">
                        Test
                    </Typography>
                </Toolbar>
            </AppBar>
        </div>
    );
}

export default withStyles(headerJss)(Header);
// export default Header;